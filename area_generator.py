import logging
import json

from retry import retry
from requests_html import HTMLSession

base_link = 'http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/index.html'


@retry(tries=3, delay=2)
def extract_provinces(link):
    session = HTMLSession()
    r = session.get(link)
    elems = r.html.find('.provincetr td a')

    provinces = []
    for elem in elems:
        name = elem.text
        link = elem.absolute_links.pop()
        provinces.append({
            'name': name,
            'link': link,
        })
    return provinces


@retry(tries=3, delay=2)
def extract_cities(link):
    session = HTMLSession()
    r = session.get(link)
    trs = r.html.find('.citytr')

    cities = []
    for tr in trs:
        code = tr.find('td:first-child a', first=True).text
        name = tr.find('td:last-child a', first=True).text
        link = tr.absolute_links.pop()
        cities.append({
            'name': name,
            'link': link,
            'code': code[:6],
        })
    return cities


@retry(tries=3, delay=2)
def extract_counties(link):
    session = HTMLSession()
    r = session.get(link)
    trs = r.html.find('.countytr')
    counties = []
    for tr in trs:
        code = tr.find('td:first-child', first=True).text
        name = tr.find('td:last-child', first=True).text
        # link = tr.absolute_links
        county = {
            'name': name,
            'code': code[:9],
        }
        # if link:
        #     county['link'] = link.pop()
        counties.append(county)
    return counties


def generate_areas():
    area = {'provinces': []}

    provinces = []
    try:
        provinces = extract_provinces(base_link)
    except Exception as e:
        logging.error(e)

    for province in provinces:
        logging.info(province)
        citeis = []
        try:
            cities = extract_cities(province['link'])
            province['code'] = cities[0]['code'][0:3]
            del province['link']
            province['cities'] = cities
        except Exception as e:
            logging.error(e)

        for city in cities:
            logging.info(city)
            try:
                counties = extract_counties(city['link'])
                del city['link']
                city['counties'] = counties
            except Exception as e:
                logging.error(e)

        area['provinces'].append(province)

    with open('area.json', 'w', encoding='utf-8') as fp:
        json.dump(area, fp, ensure_ascii=False, indent=4)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    generate_areas()
